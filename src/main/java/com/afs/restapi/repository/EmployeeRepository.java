package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    List<Employee> findAllByGender(String gender);

    List<Employee> findAllByAgeBefore(Integer age);

    List<Employee> findAllByAgeBetween(Integer minAge, Integer maxAge);

    List<Employee> findAllByNameContaining(String name);
}
